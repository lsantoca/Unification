module Types where

import Text.Printf (printf)
import Data.List (intercalate,union)  
import Control.Monad

data FunSymbol = FunSymbol {
  name ::String,
  arity :: Int
           } deriving (Eq,Show)

data Variable = Variable {
  name' :: String,
  index :: Int
  } deriving (Eq)

instance Show Variable where
  show var = let
    postfix = if index var == 0 then "" else show (index var)
    in
      (name' var) ++ postfix
      
  
data Term = Var Variable
          | Fun FunSymbol [Term] deriving (Eq)
instance Show Term where
  show (Var x) = show x
  show (Fun f terms) = (name f) ++"("++intercalate "," (map show terms) ++ ")"
            

type Language = [(String,Int)]

isCoherent :: Language -> Either String ()
isCoherent [] = Right ()
isCoherent ((f,ar):rest) = do
  _ <- isCoherent rest 
  case lookup f rest of
          Nothing -> Right ()
          Just n -> if  n /= ar then
                      Left $ printf
                      "symbol %s is used with different arities: %d and %d"
                      f ar n 
                    else
                      Right ()

areCoherent :: [Language] -> Either String ()
areCoherent = isCoherent . concat 

language :: Term -> Either String Language
language (Var _ ) = Right []
language (Fun symbol terms) =
  let
    lterms = length terms
    ar = arity symbol
    nm = name symbol
  in
    if length terms /= arity symbol then
      Left $ printf
      "Arity of symbol %s (%d) does not match number of terms (%d)"
      nm ar lterms
    else
      do
        language <- (fmap concat) (mapM language terms)
        isCoherent ((nm,ar):language)
        return (union [(nm,ar)] language)

isCorrect :: Term -> Bool
isCorrect term = case language term of
  Right _ -> True
  Left _ -> False

-- Substitutions
type Binding = (Variable,Term)
newtype Substitution = Subst [Binding]
instance Show Substitution where
  show (Subst bindings) = "[ "++ pairs ++ " ]"
    where
      pairs = intercalate ", " $ map f bindings
      f (x,t) = show t ++"/"++show x
      
  


lift :: [Binding] -> Substitution
lift = Subst 
getBindings :: Substitution -> [Binding]
getBindings (Subst list) = list

identity :: Substitution
identity = lift []
(@@) :: Term -> Substitution -> Term
(Var x) @@ subst = case lookup x (getBindings subst) of
  Nothing -> Var x
  Just t -> t
(Fun f terms) @@ subst = Fun f (map (@@subst) terms)

(@@@) :: Substitution -> Substitution -> Substitution
s1 @@@ s2 = lift $ (map (\(x,t)-> (x,t@@s2)) (getBindings s1))
  ++ [(y,s) | (y,s) <- getBindings s2, lookup y (getBindings s1) == Nothing]

(|->) :: Variable -> Term -> Substitution
x |-> t = lift [(x,t)]

occurs :: Variable -> Term -> Bool
occurs x (Var y) = x == y
occurs x (Fun f terms) = any (occurs x) terms

