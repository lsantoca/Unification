module Parser where
import Types
import Data.Char
import Text.Parsec
import Text.Parsec.Char
type Parser a = Parsec String () a

begVariable = "xyzuw"

term,variable,composedTerm :: Parser Term
term = variable <|> composedTerm

variable = do
  first <- oneOf begVariable
  rest <- many letter
  digits <- many digit
  let idx = if null digits then 0 else ((read digits)::Int)
  return (Var (Variable {name'=(first:rest),index=idx}))

composedTerm = do
  f <- funSymbolString
  terms <- parList term
  let symbol = FunSymbol {name=f,arity=length terms}
  return (Fun symbol terms)

funSymbolString = do
  first <- noneOf begVariable
  letters <- many letter
  return (first:letters)

parList :: Parser a -> Parser [a]
parList parser = do
  char '('
  list <- parser `sepBy` (char ',')
  char ')'
  return list

pair :: Parser a -> Parser (a,a)
pair parser = do
  char '('
  first <- parser
  char ','
  second <- parser
  char ')'
  return (first,second)

parseTermFromString :: String -> Either ParseError Term
parseTermFromString string = parse term "" stream
  where
    stream = filter ( not . isSpace ) string


parseProblemFromString :: String -> Either ParseError (Term,Term)
parseProblemFromString string = parse (pair term) "" stream
  where
    stream = filter ( not . isSpace ) string
