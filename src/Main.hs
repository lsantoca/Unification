import Control.Exception
import Options.Applicative
import Data.Semigroup ((<>))
import Control.Monad

import Types
import Unif
import Parser(parseTermFromString)

data Options = Options
              { firstTerm      :: String
              , secondTerm      :: String
              }

options :: Parser Options
options = Options
      <$> strOption
      (long "firstTerm"
       <> metavar "FIRSTTERM"
       <> value ""
       <> help "First term to be unified" )
      <*> strOption
      (long "secondTerm"
       <> metavar "SECONDTERM"
       <> value ""
       <> help "Second term to be unified" )

main :: IO ()
main = runWithOptions 
  =<< execParser opts
  where
    opts = info (options <**> helper)
      ( fullDesc
     <> progDesc "Execute the unification algorithm on two terms"
     <> header "Demo unification algorithm, by Luigi Santocanale" )

runWithOptions :: Options -> IO ()
runWithOptions (Options str1 str2) =
  (
  do
    str1'<- if(null str1) then promptAndGet "First term: " else return str1
    str2'<- if(null str2) then promptAndGet "Second term: " else return str2
    term1 <- myTransformer $ parseTermFromString str1' 
    term2 <- myTransformer $ parseTermFromString str2'
    result <- myTransformer $ unify [(term1,term2)]
    putStrLn $ "\nMGU: "++  show result
  )
  `catchAny`  printError


catchAny :: IO a -> (SomeException -> IO a) -> IO a
catchAny = Control.Exception.catch

printError :: SomeException -> IO ()
printError e = do
        putStrLn $ "Error : " ++ show e

promptAndGet :: String -> IO(String)
promptAndGet prompt = do
  putStr prompt >> getLine

myTransformer :: Show a => Either a b -> IO b
myTransformer action = case action of 
  Right x -> return x
  Left err -> fail $ show err
  
    
