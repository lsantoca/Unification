module Unif where
import Text.Printf
import Types
import MyTrace

unify :: [(Term,Term)] -> Either String Substitution
unify pairs =  myTrace (putStrLn ("Input :  "++show pairs)) 
  (unify' pairs)

unify' [] = return identity
unify' ((Var x, t):rest)
  | Var x == t = unify rest
  | occurs x t = fail 
    $ printf
    "Occurrence of the variable %s in the term %s" (show x) (show t)
  | otherwise = myTrace (putStrLn ("\nSubstitution "++show (x |-> t) ++  " is on stack\n"))
      (do 
        substitution <- unify (map (\(t1,t2) -> (t1@@(x |-> t), t2@@(x |-> t))) rest)
        return ((x |-> t) @@@substitution))

unify' ((t,Var x):rest) = unify ((Var x, t):rest)
unify' ((Fun f terms1,Fun g terms2):rest)
  | f == g = unify ((zip terms1 terms2)++rest)
  | otherwise = fail
    $ printf "Not unifiable: different function symbols %s and %s" (name f) (name g)

