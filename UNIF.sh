#!/bin/sh
LIBS=-ilib
SRC=-isrc
RUNHASKELL=runhaskell

if ! [ $(which ${RUNHASKELL}) ]; then
    echo "La platforme Haskell n'est pas installée sur cet ordinateur."
    echo "Vous devez l'installer avant utiliser ce programme, voir :";
    echo "https://www.haskell.org/platform/";
   exit 1;
fi

#cd src;
${RUNHASKELL}  ${LIBS} ${SRC} src/Main.hs $1

