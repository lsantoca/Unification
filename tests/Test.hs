module Test where

import Types
import Unif


f = FunSymbol {name="f",arity=1}
f' = FunSymbol {name="f",arity=2} 
x0 = Var $ Variable {name'="x",index=0}
y0 = Var $ Variable {name'="y",index=0}
z0 = Var $ Variable {name'="z",index=0}
  
term0 = Fun f [x0,Fun f' [x0,x0]]
term1 = Fun f' [x0,Fun f [x0]]

term2 = Fun f' [x0,Fun f' [x0,x0]]
term3 = Fun f' [Fun f' [y0,y0],z0]

test1:: IO()
test1 = let
  msg = case language term1 of
    Right language -> show language
    Left err -> err
  in
    putStrLn $ show msg

main = test1
