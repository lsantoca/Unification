PROJECT=UNIF
ARCH=${shell uname -p}
OUTFILE=${PROJECT}_${ARCH}
LIBS=-ilib
SRC=-isrc
MODULES=mytrace/src/Mytraces.hs

WEBDIR=
DEMODIR=

runViaScript:
	./${PROJECT}.sh

run:
	runhaskell ${LIBS} ${SRC} src/Main.hs

buildInstall:
	ghc ${LIBS} ${SRC} src/Main.hs ; cp  src/Main bin/${OUTFILE}

test:
	runhaskell ${LIBS} ${SRC} tests/Test.hs

clean:
	rm src/*.o src/*.hi src/Main
	rm lib/*.o lib/*.hi 

installModules:
	cd lib ; git clone 'https://gitlab.com/lsantoca/mytrace.git' 

zipInstall: clean
	zip -r ${PROJECT}.zip *
	mv ${PROJECT}.zip ${WEBDIR}

web: 
	cd ${DEMODIR} ; git pull

